var express = require('express');
var router = express.Router();
var account_dal = require('../model/account_dal');
var school_dal = require('../model/school_dal');
var address_dal = require('../model/address_dal');
var company_dal = require('../model/company_dal');
var skill_dal = require('../model/skill_dal');

// View All accounts
router.get('/all', function(req, res) {
    account_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('account/accountViewAll', { 'result':result });
        }
    });
});

// View the account for the given id
router.get('/', function(req, res){
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
        account_dal.getById(req.query.account_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('account/accountViewByld', {'result': result[0], 'skill': result[1], 'school': result[2], 'company': result[3]});
            }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res) {
    account_dal.getAllAdd(function (err, result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('account/accountAdd', {'account': result[0], 'skill': result[1], 'school': result[2], 'company': result[3]});
        }
    });
});


// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.first_name == null) {
        res.send('All fields must be populated');
    }
    else if(req.query.last_name == null) {
        res.send('All fields must be populated');
    }
    else if(req.query.email == null) {
        res.send('All fields must be populated');
    }
    else if(req.query.skill_id == null) {
        res.send('All fields must be populated');
    }
    else if(req.query.school_id == null) {
        res.send('All fields must be populated');
    }
    else if(req.query.company_id == null) {
        res.send('All fields must be populated');
    }

    else {
        account_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                res.redirect(302, '/account/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.account_id == null) {
        res.send('An account id is required');
    }
    else {
        account_dal.edit(req.query.account_id, function(err, result){
            res.render('account/accountUpdate', {account: result[0][0], skill: result[1], school: result[2], company: result[3]});
        });
    }
});


router.get('/update', function(req, res) {
    console.log(req.query);
    account_dal.update(req.query, function(err, result){
        if(err){
            res.send(err);
        }
        else{
            res.redirect(302, '/account/all');}
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
        account_dal.delete(req.query.account_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/account/all');
            }
        });
    }
});

module.exports = router;